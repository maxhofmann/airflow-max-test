from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.hooks.S3_hook import S3Hook

DEFAULT_ARGS = {
    'owner': 'Airflow',
    'depends_on_past': False,
    'start_date': datetime(2020, 9, 23),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG('create_date_dimension', default_args=DEFAULT_ARGS,
          schedule_interval=timedelta(minutes=5))


def write_text_file(**kwargs):
    with open("/tmp/test.txt", "w") as fp:
        # Add file generation/processing step here, E.g.:
        fp.write('oida schau dir das an')

        # Upload generated file to Minio
        s3 = S3Hook('minio')
        s3.load_file("/tmp/test.txt",
                     key=f"my-test-file.txt",
                     bucket_name="igor-ndw")


# Create a task to call your processing function
t1 = PythonOperator(
    task_id='s3_upload_test',
    provide_context=True,
    python_callable=write_text_file,
    dag=dag
)